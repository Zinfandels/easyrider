package com.easy.easyriderapp.ui.main.more

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.easy.easyriderapp.R
import com.easy.easyriderapp.viewmodels.main.more.ListOfUserFragmentViewModel

class ListOfUserFragment : Fragment() {

    companion object {
        fun newInstance() = ListOfUserFragment()
    }

    private lateinit var viewModel: ListOfUserFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_of_user_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ListOfUserFragmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
