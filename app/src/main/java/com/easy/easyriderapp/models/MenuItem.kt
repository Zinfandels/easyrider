package com.easy.easyriderapp.models

data class MenuItem(var title: String, var icon : Int, val id: Int)