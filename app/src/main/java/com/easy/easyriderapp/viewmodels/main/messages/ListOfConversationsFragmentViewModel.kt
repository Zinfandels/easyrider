package com.easy.easyriderapp.viewmodels.main.messages

import android.app.Application
import com.easy.easyriderapp.repositories.FirebaseDatabaseRepository
import com.easy.easyriderapp.viewmodels.BaseViewModel
import javax.inject.Inject

class ListOfConversationsFragmentViewModel @Inject constructor(application: Application, val firebaseDatabaseRepository: FirebaseDatabaseRepository) : BaseViewModel(application) {

    val conversations = firebaseDatabaseRepository.fetchConversations()



}
