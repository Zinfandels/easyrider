package com.easy.easyriderapp.ui.main.events

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.easy.easyriderapp.R
import com.easy.easyriderapp.adapters.EventsListRecyclerViewAdapter
import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.services.ProgressDialogService
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.events.EventsListFragmentViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.events_list_fragment.*
import javax.inject.Inject

class EventsListFragment : Fragment() {


    lateinit var listAdapter: EventsListRecyclerViewAdapter
    private val dataLoadingProgressDialog = ProgressDialogService()


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)

        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private lateinit var viewModel: EventsListFragmentViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.events_list_fragment, container, false)
        dataLoadingProgressDialog.run {
            create(context as Context)
            setMessage("Data loading...")
            showDialog()
        }
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventsListFragmentViewModel::class.java)

    }

    val navigateInRecyclerView = {event:Event ->
        val args = Bundle()
        args.putString("EVENT_NAME", event.eventName)
        args.putString("DESCRIPTION", event.description)
        args.putString("CATEGORY", event.category)
        args.putDouble("LAT", event.loc_lat as Double)
        args.putDouble("LNG", event.loc_lng as Double)
        args.putString("DATE_STRING", "${event.day}/${event.month}/${event.year}")
        args.putString("ADMIN_NAME", event.creatorName)

        findNavController().navigate(
            R.id.eventView,
            args,
            NavOptions.Builder().setPopUpTo(R.id.eventsList, false).build()
        )
    }

    override fun onStart() {
        super.onStart()
        listIsEmptyInfo.visibility = View.VISIBLE

        viewModel.getListOfEvents.observe(this, Observer {

            dataLoadingProgressDialog.hideDialog()
            if (it.isNotEmpty()) listIsEmptyInfo.visibility = View.GONE
            listAdapter = EventsListRecyclerViewAdapter(context as Context, it,navigateInRecyclerView)

            eventsListRecyclerView?.setHasFixedSize(true)
            eventsListRecyclerView?.layoutManager = LinearLayoutManager(context)
            eventsListRecyclerView?.adapter = listAdapter

            listAdapter.notifyDataSetChanged()

        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.events_menu, menu)
    }


}
