package com.easy.easyriderapp.repositories

import androidx.lifecycle.LiveData
import com.easy.easyriderapp.models.Conversation
import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.models.Message
import com.easy.easyriderapp.models.User

interface FirebaseDatabaseRepository {

    fun createNewEvent(event : Event)
    fun saveUserSearchInfo(user : User)

    fun getCurrentUserInfo() : LiveData<User>

    fun getUsers(currentUserId:String):LiveData<MutableList<User>>

    fun getSelectedEvents(fatbike:Boolean,
                          ebike:Boolean,
                          enduro:Boolean,
                          pumptrackbmx : Boolean,
                          dh:Boolean,
                          roadbike:Boolean,
                          gravelcx:Boolean,
                          citybike:Boolean,
                          xc:Boolean,

                          userDateFrom:Long?,
                          userDateTo:Long?,

                          userLocLat : Double?,
                          userLocLng:Double?,
                          userRange : Int

                          ) : LiveData<MutableList<Event>>

    fun saveMessageToDatabase(message:Message)

    fun fetchMessages(receiverId:String): LiveData<MutableList<Message>>





    fun setMetadataForReceiverUser(receiverId:String, currentUserName: String)
    fun setMetadataForCurrentUser(receiverId : String, receiverUserName : String)

    fun fetchConversations():LiveData<MutableList<Conversation>>

}