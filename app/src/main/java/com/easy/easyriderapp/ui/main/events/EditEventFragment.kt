package com.easy.easyriderapp.ui.main.events

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.easy.easyriderapp.R
import com.easy.easyriderapp.viewmodels.main.events.EditEventFragmentViewModel

class EditEventFragment : Fragment() {

    companion object {
        fun newInstance() = EditEventFragment()
    }

    private lateinit var viewModel: EditEventFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.edit_event_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(EditEventFragmentViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
