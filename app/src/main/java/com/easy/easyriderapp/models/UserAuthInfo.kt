package com.easy.easyriderapp.models

data class UserAuthInfo (val email:String, val password:String, var firstName : String? = null, var lastName : String? =null)