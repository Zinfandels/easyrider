package com.easy.easyriderapp.dagger

import android.app.Application
import com.easy.easyriderapp.App
import com.easy.easyriderapp.dagger.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


//@Component(modules = [ApplicationModule::class, ViewsBindingModule::class, AndroidSupportInjectionModule::class])
@Singleton
@Component(modules = [ AndroidSupportInjectionModule::class, ViewsBindingModule::class, AppModule::class, ViewModelModule::class, RepositoryModule::class])
interface AppComponent {

    fun inject (app : App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}