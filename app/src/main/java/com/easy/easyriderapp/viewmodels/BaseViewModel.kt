package com.easy.easyriderapp.viewmodels

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.easy.easyriderapp.App
import com.easy.easyriderapp.utils.SingleLiveEvent


abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {
    val startActivity: SingleLiveEvent<Intent> = SingleLiveEvent()

    fun context(): Context = getApplication<App>().applicationContext
    val closeActivity: SingleLiveEvent<Void> = SingleLiveEvent()

    val isProgressDialog: MutableLiveData<Boolean> = SingleLiveEvent()
    val showDialog: MutableLiveData<Int> = SingleLiveEvent()

//    val errorDialog: MutableLiveData<Int> = SingleLiveEvent()
//    val permissionRequest: SingleLiveEvent<String> = SingleLiveEvent()
//
//    fun context(): Context = getApplication<App>().applicationContext
//
    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    }

}