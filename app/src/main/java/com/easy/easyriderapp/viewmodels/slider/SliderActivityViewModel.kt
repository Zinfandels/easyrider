package com.easy.easyriderapp.viewmodels.slider

import android.app.Application
import android.content.Intent
import com.easy.easyriderapp.repositories.FirebaseAuthRepository
import com.easy.easyriderapp.ui.MainActivity
import com.easy.easyriderapp.ui.auth.LoginActivity
import com.easy.easyriderapp.ui.auth.RegisterActivity
import com.easy.easyriderapp.viewmodels.BaseViewModel
import javax.inject.Inject

class SliderActivityViewModel @Inject constructor (application : Application, val firebaseAuthRepository: FirebaseAuthRepository): BaseViewModel(application){


    fun navigateToLogIn(){
        val intent = Intent(context(), LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity.value = intent
        closeActivity.call()
    }

    fun navigateToRegister(){
        val intent = Intent(context(), RegisterActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity.value = intent
        closeActivity.call()
    }

    fun navigateToMainView(){

        firebaseAuthRepository.getCurrentLoggedUserId()?.let {
            val intent = Intent(context(), MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity.value = intent
            closeActivity.call()
        }

    }



}
