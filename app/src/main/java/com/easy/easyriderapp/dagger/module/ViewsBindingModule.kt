package com.easy.easyriderapp.dagger.module

import com.easy.easyriderapp.ui.MainActivity
import com.easy.easyriderapp.ui.auth.LoginActivity
import com.easy.easyriderapp.ui.auth.RegisterActivity
import com.easy.easyriderapp.ui.main.events.EventViewFragment
import com.easy.easyriderapp.ui.main.events.add.AddEventFragment
import com.easy.easyriderapp.ui.main.events.add.ChooseLocationOnMapFragment
import com.easy.easyriderapp.ui.main.events.EventsListFragment
import com.easy.easyriderapp.ui.main.events.map.EventsMapFragment
import com.easy.easyriderapp.ui.main.messages.ChatFragment
import com.easy.easyriderapp.ui.main.messages.CreateNewConversationFragment
import com.easy.easyriderapp.ui.main.messages.ListOfConversationsFragment
import com.easy.easyriderapp.ui.main.more.MoreFragment
import com.easy.easyriderapp.ui.main.more.SearchSettingsFragment
import com.easy.easyriderapp.ui.slider.SliderActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewsBindingModule {

//

//    (modules = [SliderActivityModule::class])
    @ContributesAndroidInjector
    internal abstract fun provideSliderAcitvity() : SliderActivity

    @ContributesAndroidInjector
    internal abstract fun priovideLoginActivity() : LoginActivity

    @ContributesAndroidInjector
    internal abstract fun priovideRegisterActivity() : RegisterActivity

    @ContributesAndroidInjector
    internal abstract fun provideMainActivity() : MainActivity

    @ContributesAndroidInjector
    internal abstract fun provideAddEventFragment() : AddEventFragment

    @ContributesAndroidInjector
    internal abstract fun provideChooseLocationOnMapFragment () : ChooseLocationOnMapFragment

    @ContributesAndroidInjector
    internal abstract fun provideListEventsFragment () : EventsListFragment

    @ContributesAndroidInjector
    internal abstract fun provideMapEventsFragment () : EventsMapFragment

    @ContributesAndroidInjector
    internal abstract fun provideMoreFragment () : MoreFragment

    @ContributesAndroidInjector
    internal abstract fun provideSearchSettingsFragment () : SearchSettingsFragment

    @ContributesAndroidInjector
    internal abstract fun provideEventViewFragment () : EventViewFragment

    @ContributesAndroidInjector
    internal abstract fun provideListOfConvesationsFragment () : ListOfConversationsFragment

    @ContributesAndroidInjector
    internal abstract fun provideCreateNewConversationFragment () : CreateNewConversationFragment

    @ContributesAndroidInjector
    internal abstract fun provideChatFragment () : ChatFragment

//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [TasksModule::class])
//    internal abstract fun tasksActivity(): TasksActivity
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [AddEditTaskModule::class])
//    internal abstract fun addEditTaskActivity(): AddEditTaskActivity
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [StatisticsModule::class])
//    internal abstract fun statisticsActivity(): StatisticsActivity
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = [TaskDetailModule::class])
//    internal abstract fun taskDetailActivity(): TaskDetailActivity
}
