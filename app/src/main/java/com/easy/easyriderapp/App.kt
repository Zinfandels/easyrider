package com.easy.easyriderapp

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.HasActivityInjector
import android.app.Activity
import com.easy.easyriderapp.dagger.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject


class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }


    override fun onCreate() {
        super.onCreate()


        DaggerAppComponent.builder().application(this).build().inject(this)

    }



}

