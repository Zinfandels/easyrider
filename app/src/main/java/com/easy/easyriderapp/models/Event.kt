package com.easy.easyriderapp.models

data class  Event (val eventName : String? = "",
                   val dateInMillis : Long? = null,
                   val description : String ="",
                   val category: String? = null,
                   val categoryTag: Any? = null,

                   val loc_lat : Double? = null,
                   val loc_lng : Double? = null,

                   var address: String = "",

                   var year :Int? =null,
                   var month :Int? =null,
                   var day : Int? =null,

                   var distanceFromUser :Int? = null,
                   var creatorId:String? = null,
                   var creatorName:String? =null,
                   var eventId :String? = null
)