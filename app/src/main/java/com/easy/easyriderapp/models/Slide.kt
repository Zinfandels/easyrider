package com.easy.easyriderapp.models

data class Slide (val title: String, val iconUri: String, val desc: String, val id : Int)