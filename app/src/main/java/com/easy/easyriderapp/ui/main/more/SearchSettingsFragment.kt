package com.easy.easyriderapp.ui.main.more

import android.app.DatePickerDialog
import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.*
import com.easy.easyriderapp.R
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.more.SearchSettingsFragmentViewmodel
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.search_settings_fragment.*
import javax.inject.Inject
import com.google.android.libraries.places.api.Places;
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.easy.easyriderapp.models.User
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*


class SearchSettingsFragment : Fragment(), PlaceSelectionListener,
    OnMapReadyCallback {

    lateinit var viewModel: SearchSettingsFragmentViewmodel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    var autocompleteFragment: AutocompleteSupportFragment? = null
    lateinit var mapView: MapView
    var googleMap: GoogleMap? = null

    val dateFromCalendar = Calendar.getInstance()
    val dateToCalendar = Calendar.getInstance()

    var loc_lat: Double? = null
    var loc_lng: Double? = null
    var address: String? = null

    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        setHasOptionsMenu(true)
        Places.initialize(context, "AIzaSyDBt-BRRo-jnfhDBQDmbrelk5lot8ZeAjw")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_settings_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchSettingsFragmentViewmodel::class.java)

        bindObservables()

        context?.let {

            mapView = view?.findViewById(R.id.userLocationMap) as MapView
            mapView.onCreate(savedInstanceState)
            mapView.onResume()
            mapView.getMapAsync(this)

            autocompleteFragment =
                childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
            (autocompleteFragment as AutocompleteSupportFragment).setPlaceFields(
                Arrays.asList(
                    Place.Field.ADDRESS,
                    Place.Field.NAME,
                    Place.Field.LAT_LNG
                )
            )
            (autocompleteFragment as AutocompleteSupportFragment).setOnPlaceSelectedListener(this)

            val dateFromSetListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                // set beginning of day
                dateFromCalendar.set(year, month, dayOfMonth, 1,0)
                viewModel.dateFromHasChanged.value =
                    dayOfMonth.toString() + "/" + (month + 1).toString() + "/" + year.toString()
            }

            val dateToSetListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                // set end of day


                dateToCalendar.set(year, month, dayOfMonth, 23,0)
                viewModel.dateToTextHasChanged.value =
                    dayOfMonth.toString() + "/" + (month + 1).toString() + "/" + year.toString()
            }

            val dateValidationHelper = SearchSettingsDateValidation(
                context as Context,
                dateFromCalendar,
                dateToCalendar,
                dateFromCardView,
                dateToCardView,
                dateFromSetListener,
                dateToSetListener
            )
            dateValidationHelper.setDataCardViewListener()

            GPSButtonInSearchSettings.setOnClickListener {
                viewModel.getGPSLocation()
            }

            distancePointer.setOnSeekBarChangeListener(
                object : SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                        // seekBar doesn't allow set up a minimum value, it is always 0, so we add value 5 to result
                        viewModel.userRangeHasChanged.value = progress + 5
                    }

                    override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                    override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    }
                })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_settings_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.saveChanges->{
                viewModel.saveUserSearchSettings(User(
                    fatbike = fatBikeCheckBox.isChecked,
                    ebike = eBikeCheckBox.isChecked,
                    enduro = enduroCheckBox.isChecked,

                    pumptrackbmx = pumptrackCheckBox.isChecked,
                    dh = DHCheckBox.isChecked,
                    roadbike = roadBikeCheckBox.isChecked,

                    gravelcx = gravelCheckBox.isChecked,
                    citybike = cityBikeCheckBox.isChecked,
                    xc = XCCheckBox.isChecked,

                    dateFrom = dateFromCalendar.timeInMillis,
                    dateTo = dateToCalendar.timeInMillis,
                    range = distancePointer.progress + 5,

                    loc_lat = loc_lat,
                    loc_lng = loc_lng,
                    address = address

                ))

                Toast.makeText(context, "Changes has been saved!", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.moreFragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPlaceSelected(p0: Place) {
        if (p0.latLng != null && p0.address != null) {
            loc_lat = (p0.latLng as LatLng).latitude
            loc_lng = (p0.latLng as LatLng).longitude
            address = p0.address

            viewModel.autocompleteLocationHaveBeenSet(p0.latLng as LatLng, p0.address as String)
        }
    }

    override fun onError(p0: Status) {
    }

    fun bindObservables() {
        viewModel.dateFromHasChanged.observe(this, Observer {
            dateFromTextView.setText(it)
        })
        viewModel.dateToTextHasChanged.observe(this, Observer {
            dateToTextView.setText(it)
        })

        viewModel.locationHaveBeenSet.observe(this, Observer {

            userLocationMap.visibility = View.VISIBLE
            addressTextView.visibility = View.VISIBLE

            addressTextView.setText(it.address)

            loc_lat = it.location.latitude
            loc_lng = it.location.longitude
            address = it.address

            markLocationOnGoogleMap(it.location.latitude, it.location.longitude)
        })

        viewModel.userRangeHasChanged.observe(this, Observer {
            val rangeString = it.toString() + " km"
            if (it != 400) distanceTextViewInStartSettings.setText(rangeString) else distanceTextViewInStartSettings.text =
                "All Events"
        })

        viewModel.initUserValue.observe(this, Observer {
                userInfo ->

            userInfo.dateFrom?.let {
                dateFromCalendar.timeInMillis = userInfo.dateFrom as Long
                dateFromTextView.setText("${dateFromCalendar.get(Calendar.DAY_OF_MONTH)}/${dateFromCalendar.get(Calendar.MONTH) + 1}/${dateFromCalendar.get(Calendar.YEAR)}")
            }


            userInfo.dateTo?.let{
                dateToCalendar.timeInMillis = userInfo.dateTo as Long
                dateToTextView.setText("${dateToCalendar.get(Calendar.DAY_OF_MONTH)}/${dateToCalendar.get(Calendar.MONTH) + 1}/${dateToCalendar.get(Calendar.YEAR)}")

            }

            if(userInfo.loc_lat !=null && userInfo.loc_lng !=null){

                userLocationMap.visibility = View.VISIBLE
                addressTextView.visibility = View.VISIBLE
                loc_lat = userInfo.loc_lat
                loc_lng = userInfo.loc_lng
                address = userInfo.address

                addressTextView.setText(userInfo.address)
                markLocationOnGoogleMap(userInfo.loc_lat as Double, userInfo.loc_lng as Double)
            }

            distancePointer.progress = userInfo.range
            if (userInfo.range != 400) distanceTextViewInStartSettings.setText(userInfo.range.toString() + " km") else distanceTextViewInStartSettings.setText(
                "All Events"
            )
            fatBikeCheckBox.isChecked = userInfo.fatbike
            eBikeCheckBox.isChecked = userInfo.ebike
            enduroCheckBox.isChecked = userInfo.enduro

            pumptrackCheckBox.isChecked = userInfo.pumptrackbmx
            DHCheckBox.isChecked = userInfo.dh
            roadBikeCheckBox.isChecked = userInfo.roadbike

            gravelCheckBox.isChecked = userInfo.gravelcx
            cityBikeCheckBox.isChecked = userInfo.citybike
            XCCheckBox.isChecked = userInfo.xc
        })
    }

    private fun markLocationOnGoogleMap(lat: Double, lng:Double) {
        if (googleMap != null) {
            googleMap?.clear()
            googleMap?.addMarker(MarkerOptions().position(LatLng(lat, lng)))
            googleMap?.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        lat,
                        lng
                    ), 14.0f
                )
            )
        } else Handler().postDelayed(Runnable { markLocationOnGoogleMap(lat,lng) }, 50)
    }
}
