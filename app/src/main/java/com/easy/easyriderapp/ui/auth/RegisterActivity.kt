package com.easy.easyriderapp.ui.auth

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.UserAuthInfo
import com.easy.easyriderapp.services.ProgressDialogService
import com.easy.easyriderapp.ui.BaseActivity
import com.easy.easyriderapp.ui.MainActivity
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.auth.RegisterActivityViewModel
import com.google.firebase.auth.FirebaseAuth
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_register.*
import javax.inject.Inject

class RegisterActivity : BaseActivity<RegisterActivityViewModel>() {
    val firebaseAuthentication = FirebaseAuth.getInstance()


    val registerProgressDialog = ProgressDialogService()


    override lateinit var viewModel: RegisterActivityViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    override fun onCreate(savedInstanceState: Bundle?) {

        AndroidInjection.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RegisterActivityViewModel::class.java)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        viewModel.isNewUserCreatedSuccesfully.observe(this, Observer {
            registerProgressDialog.hideDialog()
            if (it.isSuccessful){
                Toast.makeText(this, "Account has been created!", Toast.LENGTH_SHORT).show()
                updateUi()
                viewModel.navigateToApp()
            } else {
                Toast.makeText(this, "Authentication failed!", Toast.LENGTH_SHORT).show()
            }
        })

















        buttonSignUp.setOnClickListener {
            createUser()

        }
    }

    fun createUser() {

        val emailText = email.text.toString()
        val firstNameText = firstName.text.toString()
        val lastNameText = lastName.text.toString()
        val password = password.text.toString()
        val passwordConfirmation = passwordConfirmation.text.toString()


        if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            Toast.makeText(this, "Email is invalid", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(firstNameText)) {
            Toast.makeText(this, "First name is required", Toast.LENGTH_SHORT).show()
        } else if (password.length < 6) {
            Toast.makeText(this, "Minimum lenght of password is 6", Toast.LENGTH_SHORT).show()
        } else if (password != passwordConfirmation)
            Toast.makeText(this, "Passwords aren't the same!", Toast.LENGTH_SHORT).show()

        else {
            registerProgressDialog.run {
                create(this@RegisterActivity)
                setMessage("Register in progress...")
                showDialog()
            }

            viewModel.registerInfo.value = UserAuthInfo(emailText, password, firstNameText,lastNameText)

        }
    }

    fun updateUi() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }
}
