package com.easy.easyriderapp.ui.main.events.map

import android.graphics.drawable.Drawable
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import com.google.android.gms.maps.model.Marker

abstract class OnInfoWindowElemTouchListener(view: View, bgDrawableNormal: Drawable, bgDrawablePressed: Drawable) : View.OnTouchListener {
    private val view: View
    private val bgDrawableNormal: Drawable
    private val bgDrawablePressed: Drawable
    private val handler = Handler()
    lateinit var markerOfEvent: Marker
    var pressed = false
    private val confirmClickRunnable = object : Runnable {
        public override fun run() {
            if (endPress()) {
                onClickConfirmed(view, markerOfEvent)
            }
        }
    }

    init {
        this.view = view
        this.bgDrawableNormal = bgDrawableNormal
        this.bgDrawablePressed = bgDrawablePressed
    }

    fun setMarker(marker: Marker) {
        this.markerOfEvent = marker
    }

    override fun onTouch(vv: View, event: MotionEvent): Boolean {

        if ((0 <= event.getX() && event.getX() <= view.getWidth() &&
                    0<= event.getY() && event.getY() <= view.getHeight())) {


            when (event.getActionMasked()) {
                MotionEvent.ACTION_DOWN -> startPress()
                // We need to delay releasing of the view a little so it shows the pressed state on the screen
                MotionEvent.ACTION_UP -> handler.postDelayed(confirmClickRunnable, 150)
                MotionEvent.ACTION_CANCEL -> endPress()
                else -> {
                }
            }
        }

        else {
            // If the touch goes outside of the view's area
            // (like when moving finger out of the pressed button)
            // just release the press
            endPress()
        }
        return false
    }

    private fun startPress() {
        if (!pressed) {
            pressed = true
            handler.removeCallbacks(confirmClickRunnable)
//            view.setBackground(bgDrawablePressed)
            if (markerOfEvent != null)
                markerOfEvent.showInfoWindow()
        }
    }

    private fun endPress(): Boolean {
        if (pressed) {
            this.pressed = false
            handler.removeCallbacks(confirmClickRunnable)
//            view.setBackground(bgDrawableNormal)
            if (markerOfEvent != null)
                markerOfEvent.showInfoWindow()
            return true
        } else
            return false
    }

    /**
     * This is called after a successful click
     */
    protected abstract fun onClickConfirmed(v: View, marker: Marker)
}