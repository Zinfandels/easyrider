package com.easy.easyriderapp.models

data class User(
    var firstName: String? = null,
    var lastName: String? = null,
    var nickname: String? = null,
    var email: String? = null,




    var fatbike: Boolean = true,
    var ebike: Boolean = true,
    var enduro: Boolean = true,

    var pumptrackbmx: Boolean = true,
    var dh: Boolean = true,
    var roadbike: Boolean = true,

    var gravelcx: Boolean = true,
    var citybike: Boolean = true,
    var xc: Boolean = true,





    var dateFrom: Long? = null,
    var dateTo: Long? = null,

    var range: Int = 400,

    var loc_lat: Double? = null,
    var loc_lng: Double? = null,
    var address: String? =null,

    var userId : String? =null

)