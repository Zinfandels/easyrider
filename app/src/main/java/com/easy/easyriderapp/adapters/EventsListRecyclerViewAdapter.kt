package com.easy.easyriderapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.Event

class EventsListRecyclerViewAdapter(var context: Context, var mutableListOfEvents: MutableList<Event>, val navigationFunction: (event:Event) -> Unit) : RecyclerView.Adapter<EventsListRecyclerViewAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(context).inflate(R.layout.events_list_item, parent, false)
        v.findViewById<TextView>(R.id.eventDescTextView).maxLines = 2
        val vHolder = MyViewHolder(v)

        vHolder.eventsListItem.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                navigationFunction(Event(
                    eventName = vHolder.eventName.text.toString(),
                    description = vHolder.longDescription,
                    category = vHolder.eventCategory.text.toString(),
                    loc_lat = vHolder.lattitude,
                    loc_lng = vHolder.longitude,
                    day = vHolder.eventDay,
                    month = vHolder.eventMonth,
                    year = vHolder.eventYear,
                    creatorName = vHolder.adminName
                ))

            }
        })
        return vHolder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.eventName.setText(mutableListOfEvents.get(position).eventName)

        holder.eventDay = mutableListOfEvents.get(position).day
        holder.eventMonth = mutableListOfEvents.get(position).month
        holder.eventYear = mutableListOfEvents.get(position).year

        val dateString = "${holder.eventDay}/${ holder.eventMonth}/${holder.eventYear}"

        holder.eventDate.setText(dateString)
        holder.eventCategory.setText(mutableListOfEvents.get(position).category)

        holder.longDescription = mutableListOfEvents.get(position).description
        holder.eventDescriptionTextView.setText(holder.longDescription)

        val distanceFromUser = mutableListOfEvents.get(position).distanceFromUser
        holder.eventDistance.setText(distanceFromUser.toString() + " km")

        holder.creatorId = mutableListOfEvents.get(position).creatorId as String
        holder.lattitude = mutableListOfEvents.get(position).loc_lat

        holder.location = mutableListOfEvents.get(position).address

        holder.longitude = mutableListOfEvents.get(position).loc_lng
        holder.eventId = mutableListOfEvents.get(position).eventId as String

        holder.adminName = mutableListOfEvents.get(position).creatorName


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val eventsListItem: ConstraintLayout = itemView.findViewById(R.id.events_list_item)


        var adminName : String? = null
        lateinit var eventId: String
        val eventCategory: TextView = itemView.findViewById(R.id.eventCategoryTextView)
        lateinit var creatorId: String
        val eventDate: TextView = itemView.findViewById(R.id.eventDateEditTextInEditEvent)
        var eventDay :Int? = null
        var eventMonth :Int? = null
        var eventYear :Int? = null
        val eventDescriptionTextView: TextView = itemView.findViewById(R.id.eventDescTextView)
        lateinit var longDescription : String
        var lattitude: Double? = null
        var location: String? = null
        var longitude: Double? = null
        val eventName = itemView.findViewById<TextView>(R.id.categoryInStartSettingsTextView)
        val eventDistance: TextView = itemView.findViewById(R.id.eventDistanceTextView)

    }

    override fun getItemCount(): Int {
        return mutableListOfEvents.size
    }

    fun clearItems (){
        mutableListOfEvents.clear()
        notifyDataSetChanged()
    }

    fun updateData (newList: MutableList<Event>){
        mutableListOfEvents = newList
        notifyDataSetChanged()
    }


}
