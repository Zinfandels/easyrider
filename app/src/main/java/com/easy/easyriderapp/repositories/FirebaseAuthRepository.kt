package com.easy.easyriderapp.repositories

import com.easy.easyriderapp.models.UserAuthInfo
import com.easy.easyriderapp.utils.SingleLiveEvent
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult

interface FirebaseAuthRepository {


    fun getCreatedUserAuthResult () : SingleLiveEvent<Task<AuthResult>>
    fun getLogInResult () : SingleLiveEvent<Task<AuthResult>>

    fun getCurrentLoggedUserId () : String?


    fun createNewUser(userInfo:UserAuthInfo) : SingleLiveEvent<Task<AuthResult>>
    fun logInWithEmailAndPassword(userInfo:UserAuthInfo) : SingleLiveEvent<Task<AuthResult>>


}
