package com.easy.easyriderapp.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.easy.easyriderapp.models.Slide
import com.easy.easyriderapp.ui.slider.SliderFragment


class SliderPagerAdapter(fragmentManager: FragmentManager, private val slides: List<Slide>) :
    FragmentStatePagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment {
        return SliderFragment.newInstance(slides[position])
    }

    override fun getCount(): Int {

        return slides.size
    }

}
