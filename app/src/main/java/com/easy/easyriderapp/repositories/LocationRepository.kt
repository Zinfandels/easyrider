package com.easy.easyriderapp.repositories

import android.location.Location
import com.easy.easyriderapp.models.ExtendedLocation
import io.reactivex.Observable

interface LocationRepository {

    fun getLocation() : Observable<Location>
    fun getExactLocation(location: Location) : Observable<ExtendedLocation>
    fun removeUpdates()
}