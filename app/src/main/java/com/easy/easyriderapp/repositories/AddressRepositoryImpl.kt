package com.easy.easyriderapp.repositories

import android.content.Context
import android.location.Location
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import io.reactivex.Observable
import org.json.JSONObject

class AddressRepositoryImpl(var context: Context) : AddressRepository {


    override fun getAddress(location: Location): Observable<String> {
        return Observable.create {
            val lat = location?.latitude.toString()
            val lng = location?.longitude.toString()
            val requestQueque = Volley.newRequestQueue(context)
            val requestURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=AIzaSyC3hrtpa1FuVpqI8F_5QU1618WhQ38R20I"
            val listener = Response.Listener<JSONObject> { response ->
                val addressFromGPS = parseResponse(response)
                it.onNext(addressFromGPS)
            }

            val errorListener: Response.ErrorListener = object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError?) {
                    it.onError(Throwable("GPS search error, try again later..."))
                }
            }
            val request = JsonObjectRequest(requestURL, JSONObject(), listener, errorListener)
            requestQueque.add(request)
        }
    }


    private fun parseResponse(response: JSONObject): String {
        var addressFromGPS = ""
        val componentsOfAddress = response.getJSONArray("results")?.getJSONObject(0)?.getJSONArray("address_components")
        if (componentsOfAddress != null) {
            for (i in 0 until componentsOfAddress.length()) {
                val types = componentsOfAddress.getJSONObject(i).getJSONArray("types")[0]
                if (types == "route") {
                    val nameOfStreet = componentsOfAddress.getJSONObject(i).getString("short_name")
                    addressFromGPS += "$nameOfStreet, "
                }
                if (types == "locality") {
                    val locality = componentsOfAddress.getJSONObject(i).getString("short_name")
                    addressFromGPS += "$locality, "
                }
                if (types == "country") {
                    val country = componentsOfAddress.getJSONObject(i).getString("short_name")
                    addressFromGPS += country
                }
            }
        }
        return addressFromGPS
    }






}