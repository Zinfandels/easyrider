package com.easy.easyriderapp.ui.slider


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.Slide


class SliderFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slider, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val titleTextView = view.findViewById<TextView>(R.id.titleTextView)
        val descriptionTextView = view.findViewById<TextView>(R.id.descriptionTextView)
        val iconImageView = view.findViewById<ImageView>(R.id.iconInSlider)

        val args = arguments
        if(args !=null && activity!=null){
            val slideId = args.getInt(SLIDE_ID)

            titleTextView.text = slidesArray[slideId].title
            descriptionTextView.text = slidesArray[slideId].desc

            Glide.with(this).load(resources.getIdentifier(slidesArray[slideId].iconUri, "drawable", context?.packageName)).into(iconImageView)
        }
    }
    companion object {
        fun newInstance(slide: Slide): SliderFragment {

            val args = Bundle()
            args.putInt(SLIDE_ID, slide.id)

            val fragment = SliderFragment()
            fragment.arguments = args
            return fragment
        }
    }

}

