package com.easy.easyriderapp.ui

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.lifecycle.Observer
import com.easy.easyriderapp.viewmodels.BaseViewModel
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity<T : BaseViewModel> : DaggerAppCompatActivity() {

    public abstract val viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        bindObservables()


    }


    override fun onCreateView(parent: View?, name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(parent, name, context, attrs)
    }





    protected open fun bindObservables() {
        viewModel.startActivity.observe(this, Observer { startActivity(it) })
        viewModel.closeActivity.observe(this, Observer { finish() })
        viewModel.showDialog.observe(this, Observer { showDialog(it) })


    }



}