package com.easy.easyriderapp.models

data class Message(
    var fromId: String? = null,
    var text: String? = null,
    var timestamp: Long? = null,
    var toId: String? = null,
    var messageId:String? =null

)
