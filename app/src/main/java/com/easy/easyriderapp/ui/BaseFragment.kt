package com.easy.easyriderapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.easy.easyriderapp.viewmodels.BaseViewModel

abstract class BaseFragment <T: BaseViewModel>: Fragment(){

    protected abstract val viewModel: T

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bindObservables()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    protected open fun bindObservables() {
//        viewModel.showDialog.observe(this, Observer { showDialog(it) })

    }


}