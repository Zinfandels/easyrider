package com.easy.easyriderapp.utils

import android.content.Context
import android.location.Location
import com.google.android.gms.maps.model.LatLng


object DistancesUtils {

        fun calculateDistanceBeetwenPointsOnMap(StartP: LatLng, EndP: LatLng): Float {
            val results = FloatArray(1)
            Location.distanceBetween(
                StartP.latitude, StartP.longitude,
                EndP.latitude, EndP.longitude,
                results
            )
            return results[0]/1000
        }

    fun getPixelsFromDp(context: Context, dp: Float): Int {

        val scale = context.getResources().getDisplayMetrics().density;
        return (dp * scale + 0.5f).toInt()
    }

}