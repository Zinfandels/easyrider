package com.easy.easyriderapp.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.easy.easyriderapp.models.Conversation
import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.models.Message
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.utils.DistancesUtils.calculateDistanceBeetwenPointsOnMap
import com.easy.easyriderapp.utils.FirebaseValueEventLiveData
import com.easy.easyriderapp.utils.categoryConst
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.FirebaseDatabase
import javax.inject.Inject

class FirebaseDatabaseRepositoryImpl @Inject constructor(val firebaseAuthRepository: FirebaseAuthRepository) :
    FirebaseDatabaseRepository {


    val firebaseDatabaseRefernece = FirebaseDatabase.getInstance()

    val usersDatabaseRefernece = firebaseDatabaseRefernece.getReference("users")

    val currentUserDatabaseReference =
        usersDatabaseRefernece.child(firebaseAuthRepository.getCurrentLoggedUserId() as String)

    val eventsDatabaseReference = firebaseDatabaseRefernece.getReference("events")

    val messagesDatabaseReference = firebaseDatabaseRefernece.getReference("messages")

    override fun saveUserSearchInfo(user: User) {
        val mutableMapSearchInfo: MutableMap<String, Any?> = mutableMapOf()
        mutableMapSearchInfo.run {

            user.address?.let { put("address", user.address as Any?) }
            user.loc_lat?.let { put("loc_lat", user.loc_lat as Any?) }
            user.loc_lng?.let { put("loc_lng", user.loc_lng as Any?) }

            user.range?.let { put("range", user.range as Any?) }

            user.dateFrom?.let { put("dateFrom", user.dateFrom as Any?) }
            user.dateTo?.let { put("dateTo", user.dateTo as Any?) }

            user.fatbike?.let { put("fatbike", user.fatbike as Any?) }
            user.ebike?.let { put("ebike", user.ebike as Any?) }
            user.enduro?.let { put("enduro", user.enduro as Any?) }

            user.pumptrackbmx?.let { put("pumptrackbmx", user.pumptrackbmx as Any?) }
            user.dh?.let { put("dh", user.dh as Any?) }
            user.roadbike?.let { put("roadbike", user.roadbike as Any?) }

            user.gravelcx?.let { put("gravelcx", user.gravelcx as Any?) }
            user.citybike?.let { put("citybike", user.citybike as Any?) }
            user.xc?.let { put("xc", user.xc as Any?) }
        }

        currentUserDatabaseReference.updateChildren(mutableMapSearchInfo)
    }

    override fun createNewEvent(event: Event) {

        val eventToMakeUp = event
        val newEventReference = firebaseDatabaseRefernece.getReference("events").push()
        val eventId = newEventReference.key
        eventToMakeUp.eventId = eventId

        newEventReference.setValue(eventToMakeUp)
    }

    override fun getCurrentUserInfo(): LiveData<User> {
        val userByMap: LiveData<User> = Transformations.map((FirebaseValueEventLiveData(
            currentUserDatabaseReference,
            true
        )), {
            it.getValue(User::class.java)
        })

        return userByMap
    }

    override fun getSelectedEvents(
        fatbike: Boolean,
        ebike: Boolean,
        enduro: Boolean,
        pumptrackbmx: Boolean,
        dh: Boolean,
        roadbike: Boolean,
        gravelcx: Boolean,
        citybike: Boolean,
        xc: Boolean,
        userDateFrom: Long?,
        userDateTo: Long?,
        userLocLat: Double?,
        userLocLng: Double?,
        userRange: Int
    ): LiveData<MutableList<Event>> {
        return Transformations.switchMap(FirebaseValueEventLiveData(eventsDatabaseReference, true)) {

            val switchMap = MutableLiveData<MutableList<Event>>()
            val listOfProperEvents = mutableListOf<Event>()

            for (event in it.children) {

                val eventInfo = event.getValue(Event::class.java)

                var isCategoryAllowed = true

                when (eventInfo?.categoryTag.toString()) {
                    categoryConst.FATBIKE.toString() -> {
                        isCategoryAllowed = fatbike
                    }
                    categoryConst.EBIKE.toString() -> {
                        isCategoryAllowed = ebike
                    }
                    categoryConst.ENDURO.toString() -> {
                        isCategoryAllowed = enduro
                    }

                    categoryConst.PUMPTRACKBMX.toString() -> {
                        isCategoryAllowed = pumptrackbmx
                    }
                    categoryConst.DH.toString() -> {
                        isCategoryAllowed = dh
                    }
                    categoryConst.ROADBIKE.toString() -> {
                        isCategoryAllowed = roadbike
                    }

                    categoryConst.GRAVELCX.toString() -> {
                        isCategoryAllowed = gravelcx
                    }
                    categoryConst.CITYBIKE.toString() -> {
                        isCategoryAllowed = citybike
                    }
                    categoryConst.XC.toString() -> {
                        isCategoryAllowed = xc
                    }
                }


//                var distanceToEvent :Float? =null
//                var stringNull :String? = "location is not set"
//
//                if(userLocLat!=null && userLocLng!=null){
//                    distanceToEvent = (DistancesUtils.calculateDistanceBeetwenPointsOnMap(LatLng(userLocLat as Double,userLocLng as Double), LatLng(eventInfo?.loc_lat  as Double ,eventInfo.loc_lng as Double)))
//
//                    stringNull = """
//
//                    | userRange $userRange
//                    | distanceToEvent ${distanceToEvent}
//                    | is distance beetwen event and user location smaler than userRange  ${(distanceToEvent < userRange)}
//                    |
//                   """
//                }
//
//                println("""
//
//                     LOG - EVENT PRE-VALIDATION
//
//                     event name : ${eventInfo?.eventName}
//                   | event category : ${eventInfo?.categoryTag}
//                   | isCategoryAllowed $isCategoryAllowed
//                   |
//                   | event date in milis ${eventInfo?.dateInMillis}
//                   |
//                   | userDateFrom $userDateTo
//                   | is event date smaller than userDateTo ${((eventInfo?.dateInMillis as Long) < (userDateTo as Long))}
//                   |
//                   | userDateFrom $userDateFrom
//                   | is event date greater than userDateTo ${((eventInfo?.dateInMillis) > (userDateFrom as Long))}
//                   |
//                   $stringNull
//                """)

                if (userDateTo != null && eventInfo?.dateInMillis != null) {
                    if (eventInfo.dateInMillis > userDateTo) {
                        continue
                    }
                }

//                println("LOG - VALIDATION DATETO IS COMPLETE")

                if (userDateFrom != null && eventInfo?.dateInMillis != null) {
                    if (eventInfo.dateInMillis < userDateFrom) {
                        continue
                    }
                }
//                println("LOG - VALIDATION DATEFROM IS COMPLETE")

                if (userLocLat != null && userLocLng != null && eventInfo?.loc_lat != null && eventInfo?.loc_lng != null) {
                    val distanceFromUser = calculateDistanceBeetwenPointsOnMap(
                        LatLng(userLocLat, userLocLng),
                        LatLng(eventInfo?.loc_lat, eventInfo.loc_lng)
                    )
                    if (userRange != 400 && distanceFromUser > userRange) {
                        continue
                    } else {
                        eventInfo.distanceFromUser = distanceFromUser.toInt()
                    }
                }

//                println("LOG - VALIDATION USER RANGE IS COMPLETE")


                if (!isCategoryAllowed) {
                    continue
                }

//                println("LOG - VALIDATION CATEGORY IS COMPLETE")

                listOfProperEvents.add(eventInfo as Event)

            }
            switchMap.value = listOfProperEvents

            return@switchMap switchMap
        }

    }

    override fun getUsers(currentUserId: String): LiveData<MutableList<User>> {
        return Transformations.switchMap(FirebaseValueEventLiveData(usersDatabaseRefernece, true)) {
            val liveData = MutableLiveData<MutableList<User>>()
            val mutableListOfUsersToReturn = mutableListOf<User>()
            val usersDataSnapshot = it.child("users")

            for (userInDs in it.children) {
                val user = userInDs.getValue(User::class.java)

                if (currentUserId != user?.userId) {
                    mutableListOfUsersToReturn.add(user as User)
                }
            }

            liveData.value = mutableListOfUsersToReturn
            return@switchMap liveData
        }
    }

    override fun saveMessageToDatabase(message: Message) {
        val messageId = messagesDatabaseReference.push().key

        val messageToMakeUp = message
        messageToMakeUp.messageId = messageId

        messagesDatabaseReference.child(messageToMakeUp.fromId.toString()).child(messageToMakeUp.toId.toString())
            .child("messages").child(messageId as String).setValue(messageToMakeUp)



        messagesDatabaseReference.child(messageToMakeUp.toId.toString()).child(messageToMakeUp.fromId.toString())
            .child("messages").child(messageId).setValue(messageToMakeUp)



    }

    override fun fetchMessages(receiverId: String): LiveData<MutableList<Message>> {
        return Transformations.map(

            FirebaseValueEventLiveData(
                messagesDatabaseReference.child(firebaseAuthRepository.getCurrentLoggedUserId().toString()).child(
                    receiverId
                ).child("messages"), false
            )
        ) {
            val mutableListOfMessages = mutableListOf<Message>()
            for (message in it.children) {
                mutableListOfMessages.add(message.getValue(Message::class.java) as Message)
            }
            return@map mutableListOfMessages
        }
    }



    override fun setMetadataForReceiverUser(receiverId:String, currentUserName: String) {
        val messageMetaData = mutableMapOf<String, Any>()
        messageMetaData.put("isConversationRead", false)
        currentUserName?.let { messageMetaData.put("receiverName", currentUserName) }

        messagesDatabaseReference.child(receiverId)
            .child(firebaseAuthRepository.getCurrentLoggedUserId().toString())
            .updateChildren(messageMetaData)
    }

    override fun setMetadataForCurrentUser(receiverId : String, receiverUserName : String) {

        val messageMetaData = mutableMapOf<String, Any>()
        messageMetaData.put("isConversationRead", true)
        receiverUserName?.let { messageMetaData.put("receiverName", receiverUserName) }

        messagesDatabaseReference.child(firebaseAuthRepository.getCurrentLoggedUserId().toString())
            .child(receiverId).updateChildren(messageMetaData)

    }


    override fun fetchConversations(): LiveData<MutableList<Conversation>> {
        return Transformations.map(
            FirebaseValueEventLiveData(
                messagesDatabaseReference.child(firebaseAuthRepository.getCurrentLoggedUserId().toString()),
                false
            )
        ) {

            val mutableListOfConversations = mutableListOf<Conversation>()

            for (conv in it.children) {
                val conversationToSave = Conversation()
                for (node in conv.children) {

                    if (node.key == "isConversationRead") conversationToSave.isRead = node.value as Boolean
                    if (node.key == "receiverName") conversationToSave.receiverName = node.value.toString()
                    if (node.key == "messages") {
                        val lastMessage = node.children.last().getValue(Message::class.java)
                        conversationToSave.lastMessage = (lastMessage as Message).text
                        conversationToSave.receiverId = conv.key
                    }
                }
                mutableListOfConversations.add(conversationToSave)
            }
            mutableListOfConversations
        }
    }


}