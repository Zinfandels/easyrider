package com.easy.easyriderapp.ui.main.messages

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.easy.easyriderapp.R
import com.easy.easyriderapp.adapters.RecyclerViewAdapterForChatMessages
import com.easy.easyriderapp.models.Message
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.messages.ChatFragmentViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.chat_fragment.*
import javax.inject.Inject

class ChatFragment : Fragment() {
    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: ChatFragmentViewModel
    lateinit var messageAdapter: RecyclerViewAdapterForChatMessages

    var receiverId : String? = null
    var receiverName : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.chat_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        isAdded ?:return

        receiverId = arguments?.getString("RECEIVER_ID")
        receiverName = arguments?.getString("RECEIVER_NICKNAME")

        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ChatFragmentViewModel::class.java)


        viewModel.setMetadataForCurrentUser(receiverId as String,receiverName as String)

        sendMessageButton.setOnClickListener {

            if (TextUtils.isEmpty(writeMessageField.text.toString())) return@setOnClickListener
            viewModel.sendMessageToDatabase(

                Message(
                    text = writeMessageField.text.toString(),
                    toId = receiverId
                )
            )

            viewModel.setMetadataForReceiverUser(receiverId as String)


            newMessageLayout.getLayoutParams().height = ConstraintLayout.LayoutParams.WRAP_CONTENT
            newMessageLayout.layoutParams = newMessageLayout.getLayoutParams()
            writeMessageField.setText("")
        }

        val dummydata= mutableListOf<Message>()
        messageAdapter = RecyclerViewAdapterForChatMessages(context as Context, dummydata, viewModel.getCurrentUserId().toString())

        val llm = LinearLayoutManager(context)
        chatMessagesRecyclerView.layoutManager = llm
        chatMessagesRecyclerView?.setHasFixedSize(true)

        chatMessagesRecyclerView.adapter = messageAdapter

        val observer = object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                llm.smoothScrollToPosition(chatMessagesRecyclerView, null, messageAdapter.itemCount)
            }
        }


        viewModel.fetchMessages(receiverId as String).observe(this, Observer {
            if(it!=null) messageAdapter.updateData(it)

        })
        messageAdapter.registerAdapterDataObserver(observer)


        chatMessagesRecyclerView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {

                val inputManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                val currentFocusedView = activity?.currentFocus

                if (currentFocusedView != null) {
                    inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
                viewModel.setMetadataForCurrentUser(receiverId as String, receiverName as String)
                return false
            }
        })


    }


}
