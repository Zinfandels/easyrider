package com.easy.easyriderapp.repositories

import android.annotation.SuppressLint
import android.app.Application
import android.location.Location
import android.os.Looper
import com.easy.easyriderapp.models.ExtendedLocation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LocationRepositoryImpl @Inject constructor(var context: Application,
                                                 var fusedLocationProviderClient: FusedLocationProviderClient,
                                                 var addressRepository: AddressRepository) : LocationRepository {

    private val locationSubject: PublishSubject<Location> = PublishSubject.create()
    private lateinit var locationRequest: LocationRequest



    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location = locationResult.locations[0]
            if (location != null) locationSubject.onNext(location) else locationSubject.onError(Throwable("Returned location is null"))
        }
    }


    @SuppressLint("MissingPermission") //TODO this is for now...
    override fun getLocation(): Observable<Location> {

        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
        return locationSubject
    }


    override fun removeUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }



    override fun getExactLocation(location: Location): Observable<ExtendedLocation> {
        return addressRepository.getAddress(location).flatMap { address ->
            prepareLocation(location, address)
        }
    }



    private fun prepareLocation(location: Location, address: String) : Observable<ExtendedLocation> {
        return Observable.create {
            it.onNext(ExtendedLocation(location, address))
        }
    }



}