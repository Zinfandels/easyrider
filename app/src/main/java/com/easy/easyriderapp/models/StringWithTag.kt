package com.easy.easyriderapp.models

class StringWithTag (val stringPart: String, val tagPart: Any){
    
    override fun toString(): String {
        return stringPart
    }
}