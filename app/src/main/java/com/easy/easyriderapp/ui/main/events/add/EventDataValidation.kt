package com.easy.easyriderapp.ui.main.events.add

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.lifecycle.MutableLiveData
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.StringWithTag
import com.easy.easyriderapp.services.DateDialogService
import com.easy.easyriderapp.utils.categoryConst
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.*

class EventDataValidation(val context: Context) {


    fun addValidationListenerForEventName(
        textInputLayout: TextInputLayout,
        textInputEditText: TextInputEditText,
        eventNameLiveData: MutableLiveData<String>
    ) {
        fun validateEventNameEditText(s: CharSequence?) {
            if (s != null) {
                if (s.length < 3) {
                    textInputLayout.setError("Give us at least 3 signs...")
                } else if (s.length > 25) {
                    textInputLayout.setError("Max length of event name is 25 signs...")

                } else {
                    textInputLayout.setError(null)
                }
            }
        }

        textInputEditText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateEventNameEditText(s)
            }
        })

        textInputEditText.onFocusChangeListener = object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (!hasFocus) {
                    eventNameLiveData.value = textInputEditText.text.toString()
                    validateEventNameEditText(textInputEditText.text)
                }
            }
        }
    }


    fun isEventNameValid(eventData: String): Boolean {
        if (eventData.length < 3) {
            Toast.makeText(context, "Event name need to have at least 3 signs...", Toast.LENGTH_SHORT).show()
            return false
        } else if (eventData.length > 25) {
            Toast.makeText(context, "Max length of event name is 25 signs...", Toast.LENGTH_SHORT).show()
            return false
        } else {
            return true
        }
    }


    fun addDateDialogListener(
        textInputLayout: TextInputLayout,
        textInputEditText: TextInputEditText,
        dateStringLiveData: MutableLiveData<String>,
        dateLongLiveData : MutableLiveData<Long>

        ) {
        textInputEditText?.keyListener = null

        val calendar = Calendar.getInstance()

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

                calendar.set(year, month, dayOfMonth)




                val dateString: String = dayOfMonth.toString() + "/" + (month + 1).toString() + "/" + year.toString()
                dateStringLiveData.value = dateString
                dateLongLiveData.value = calendar.timeInMillis
                textInputEditText.setError(null)

            }
        }

        val cancelListener = object : DialogInterface.OnCancelListener {
            override fun onCancel(dialog: DialogInterface?) {
                if (textInputEditText.text!!.isEmpty()) {
                    textInputLayout.error = "You have to choose a dateInMillis..."


                } else if (textInputEditText?.text!!.isNotEmpty()) {
                    textInputLayout.setError(null)

                }
            }
        }

        val dateDialogService = DateDialogService(context as Activity, dateSetListener, cancelListener)


        textInputEditText?.onFocusChangeListener = object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (hasFocus) {
                    dateDialogService.showDatePicker()

                    context?.let {
                        val inputManager =
                            context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        val currentFocusedView = context.currentFocus

                        inputManager.hideSoftInputFromWindow(
                            currentFocusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS
                        )
                    }

                } else if (!hasFocus && textInputEditText?.text!!.isEmpty()) {
                    textInputLayout?.error = "You have to choose dateInMillis..."


                } else if (!hasFocus && textInputEditText?.text!!.isNotEmpty()) {
                    textInputLayout?.setError(null)
                }
            }
        }

        textInputEditText?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                dateDialogService.showDatePicker()
            }
        })

    }


    fun addDescriptionChangedListener(
        eventDescriptionEditTextInAddEventView: TextInputEditText,
        descLiveData: MutableLiveData<String>
    ) {

        eventDescriptionEditTextInAddEventView.onFocusChangeListener = object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (!hasFocus) {
                    descLiveData.value = eventDescriptionEditTextInAddEventView.text.toString()
                }
            }
        }

    }

    fun addCategoryChangedListener(spinner: Spinner, categoryLiveData: MutableLiveData<Int>) {
        val arrayOfCategoryItems = ArrayList<StringWithTag>()
        arrayOfCategoryItems.run {
            add(StringWithTag("Fat Bike", categoryConst.FATBIKE))
            add(StringWithTag("E-bike", categoryConst.EBIKE))
            add(StringWithTag("Enduro", categoryConst.ENDURO))
            add(StringWithTag("Pumptrack/BMX", categoryConst.PUMPTRACKBMX))
            add(StringWithTag("DH", categoryConst.DH))
            add(StringWithTag("Road bike", categoryConst.ROADBIKE))
            add(StringWithTag("Gravel/CX", categoryConst.GRAVELCX))
            add(StringWithTag("City bike", categoryConst.CITYBIKE))
            add(StringWithTag("XC", categoryConst.XC))
        }
        val spinnerAdapter = ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, arrayOfCategoryItems)
        spinner.adapter = spinnerAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                categoryLiveData.value = position

            }
        }


    }


    fun dpToPx(dp: Int): Int {
        val density: Float = context.resources?.displayMetrics?.density as Float

        return Math.round(dp.toFloat() * density)
    }


}