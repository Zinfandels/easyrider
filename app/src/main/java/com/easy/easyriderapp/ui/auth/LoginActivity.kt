package com.easy.easyriderapp.ui.auth

import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.UserAuthInfo
import com.easy.easyriderapp.services.ProgressDialogService
import com.easy.easyriderapp.ui.BaseActivity
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.auth.LoginActivityViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseActivity<LoginActivityViewModel>() {

    override lateinit var viewModel: LoginActivityViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val loginProgressDialog = ProgressDialogService()

    override fun onCreate(savedInstanceState: Bundle?) {

        AndroidInjection.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginActivityViewModel::class.java)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel.isLogInMadeSuccesfully.observe(this, Observer {
            loginProgressDialog.hideDialog()

            if(it.isSuccessful){
                Toast.makeText(this,"Successfully logged!", Toast.LENGTH_SHORT).show()
                viewModel.navigateToApp()
            } else {
                Toast.makeText(this,"Authientication failed!", Toast.LENGTH_SHORT).show()
            }
        })

        signInByEmailButton.setOnClickListener {
            loginUser()
            loginProgressDialog.run {
                create(this@LoginActivity)
                setMessage("Login in progress...")
                showDialog()
            }
        }
    }

    fun loginUser(){
        val email = email.text.toString()
        val password = password.text.toString()

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Empty e-mail field", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Empty password field", Toast.LENGTH_SHORT).show()
        } else {
            viewModel.loginInfo.value = UserAuthInfo(email, password)

        }}

}



