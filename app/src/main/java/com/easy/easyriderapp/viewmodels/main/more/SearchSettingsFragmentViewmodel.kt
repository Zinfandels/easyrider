package com.easy.easyriderapp.viewmodels.main.more

import android.app.Application
import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.easy.easyriderapp.models.ExtendedLocation
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.repositories.FirebaseDatabaseRepository
import com.easy.easyriderapp.repositories.LocationRepository
import com.easy.easyriderapp.viewmodels.BaseViewModel
import com.google.android.gms.maps.model.LatLng

import javax.inject.Inject

class SearchSettingsFragmentViewmodel @Inject constructor(
    application: Application,
    private val locationRepository: LocationRepository,
    private val firebaseDatabaseRepository: FirebaseDatabaseRepository
) : BaseViewModel(application) {


    val dateFromHasChanged = MutableLiveData<String>()
    val dateToTextHasChanged = MutableLiveData<String>()
    val userRangeHasChanged = MutableLiveData<Int>()
    val locationHaveBeenSet = MutableLiveData<ExtendedLocation>()



    fun saveUserSearchSettings (user:User){
        firebaseDatabaseRepository.saveUserSearchInfo(user)
    }

    fun getGPSLocation() {
        val disposable = locationRepository.getLocation()
            .flatMap {
                locationRepository.getExactLocation(it)
            }
            .subscribe({
                locationHaveBeenSet.value = it
            }, {})
    }



    fun autocompleteLocationHaveBeenSet(latLng: LatLng, address: String){
        val location = Location("")

        location.latitude = latLng.latitude
        location.longitude = latLng.longitude

        locationHaveBeenSet.value = ExtendedLocation(location,address)
    }

    val initUserValue = firebaseDatabaseRepository.getCurrentUserInfo()

//    val sendUserInfoToDatabase = Transformations.switchMap(saveUserInfo, {
//        firebaseDatabase.saveUserSearchInfo(it)
//    })
}
