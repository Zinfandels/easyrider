package com.easy.easyriderapp.viewmodels.main.messages

import android.app.Application
import androidx.lifecycle.LiveData
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.repositories.FirebaseAuthRepository
import com.easy.easyriderapp.repositories.FirebaseDatabaseRepository
import com.easy.easyriderapp.viewmodels.BaseViewModel

import javax.inject.Inject

class CreateNewConversationFragmentViewModel @Inject constructor (application:Application,val firebaseDatabaseRepository: FirebaseDatabaseRepository,val firebaseAuthRepository: FirebaseAuthRepository) : BaseViewModel(application) {

    fun getUsers() : LiveData<MutableList<User>> {
        return firebaseDatabaseRepository.getUsers(firebaseAuthRepository.getCurrentLoggedUserId().toString())
    }


}

