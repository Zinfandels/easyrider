package com.easy.easyriderapp.ui.main.messages

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.easy.easyriderapp.R
import com.easy.easyriderapp.adapters.RecyclerViewAdapterNewConversation
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.messages.CreateNewConversationFragmentViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.create_new_conversation_fragment.*
import javax.inject.Inject

class CreateNewConversationFragment : Fragment() {

    private lateinit var viewModel: CreateNewConversationFragmentViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var recyclerViewAdapterForNewConversation: RecyclerViewAdapterNewConversation? =
        null


    override fun onAttach(context: Context) {

        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.create_new_conversation_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CreateNewConversationFragmentViewModel::class.java)


        val usersToList = mutableListOf<User>()

        val navigateToChat = { user:User ->
            val args = Bundle()
            args.putString("RECEIVER_ID", user.userId)
            args.putString("RECEIVER_NICKNAME", user.nickname)
            findNavController().navigate(R.id.chatFragment, args)
        }

        recyclerViewAdapterForNewConversation = RecyclerViewAdapterNewConversation(context as Context, usersToList,navigateToChat)
        newMessageUsersListRecyclerView.layoutManager = LinearLayoutManager(activity)
        newMessageUsersListRecyclerView.setHasFixedSize(true)

        newMessageUsersListRecyclerView.adapter = recyclerViewAdapterForNewConversation
        viewModel.getUsers().observe(this, Observer {
            (recyclerViewAdapterForNewConversation as RecyclerViewAdapterNewConversation).updateData(it)

        })

    }


}
