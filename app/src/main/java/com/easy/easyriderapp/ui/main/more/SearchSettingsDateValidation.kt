package com.easy.easyriderapp.ui.main.more

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import androidx.cardview.widget.CardView
import com.easy.easyriderapp.services.DateDialogService
import java.util.*

class SearchSettingsDateValidation(val context : Context,
                                   val dateFromCalendar : Calendar,
                                   val dateToCalendar : Calendar,
                                   val dateFromCardView : CardView,
                                   val dateToCardView: CardView,
                                   dateFromSetListener : DatePickerDialog.OnDateSetListener,
                                   dateToSetListener: DatePickerDialog.OnDateSetListener


) {


    fun getMinDateOfDateTo () : Long{
        return dateFromCalendar.timeInMillis - 100
    }

    fun getMaxDateOfDatefrom () : Long {
        return dateToCalendar.timeInMillis -100
    }

    val dateFromDialogService = DateDialogService(context as Activity, dateFromSetListener,null )
    val dateToDialogService = DateDialogService(context as Activity, dateToSetListener, null)

    fun setDataCardViewListener(){

        dateFromCardView.setOnClickListener {
            dateFromDialogService.maxDate = getMaxDateOfDatefrom()
            dateFromDialogService.showDatePicker()

        }
        dateToCardView.setOnClickListener {
            dateToDialogService.minDate = getMinDateOfDateTo()
            dateToDialogService.showDatePicker()
        }

    }

}